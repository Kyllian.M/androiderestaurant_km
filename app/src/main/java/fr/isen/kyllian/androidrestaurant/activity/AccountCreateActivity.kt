package fr.isen.kyllian.androidrestaurant.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.databinding.ActivityAccountCreateBinding
import fr.isen.kyllian.androidrestaurant.service.Services

private lateinit var binding : ActivityAccountCreateBinding
class AccountCreateActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountCreateBinding.inflate(layoutInflater)

        binding.warning.text = ""
        binding.button.setOnClickListener {
            if(isEverythingValid()){
                Services.accountService.sendCreateRequest(
                    this,
                    binding.firstname.text.toString(),
                    binding.lastname.text.toString(),
                    binding.address.text.toString(),
                    binding.email.text.toString(),
                    binding.password.text.toString()
                )
            }
        }

        setContentView(binding.root)
    }

    fun isEverythingValid() : Boolean{
        return areFieldsFull() && isPasswordCorrect() && isMailCorrect();
    }

    fun areFieldsFull () : Boolean{
        val ret = binding.firstname.text.isNotBlank() && binding.lastname.text.isNotBlank() && binding.address.text.isNotBlank() && binding.email.text.isNotBlank() && binding.password.text.isNotBlank()
        if(!ret)
            binding.warning.text = "Merci de remplir tous les champs"
        return ret
    }

    fun isPasswordCorrect () : Boolean{
        val ret = binding.password.text.toString().length >= 8
        if(!ret)
            binding.warning.text = "Utilisez un mot de passe d'au moins 8 characteres"
        return ret;
    }

    fun isMailCorrect () : Boolean{
        val ret = !binding.email.text.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(binding.email.text).matches()
        if(!ret)
            binding.warning.text = "Email au format incorrect"
        return ret
    }
}