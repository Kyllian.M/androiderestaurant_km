package fr.isen.kyllian.androidrestaurant.model

import java.io.Serializable

data class RequestLoginJSON (
    val id_shop : Int,
    val email : String,
    val password : String
) : Serializable