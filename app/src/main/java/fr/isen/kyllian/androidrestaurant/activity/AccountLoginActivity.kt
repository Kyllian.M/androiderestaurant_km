package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.databinding.ActivityAccountCreateBinding
import fr.isen.kyllian.androidrestaurant.databinding.ActivityAccountLoginBinding
import fr.isen.kyllian.androidrestaurant.service.Services

private lateinit var binding : ActivityAccountLoginBinding
class AccountLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountLoginBinding.inflate(layoutInflater)

        if(Services.accountService.login_info != null){
            binding.warning.text = "Vous etes deja loggé en tant que " + Services.accountService.login_info!!.data.email + " appuyer sur ce bouton vous logoutera"
        }

        binding.createAccount.setOnClickListener {
            Log.i("Logs","Clicked On create account, opening view AccountCreateActivity");
            val intent = Intent(this,AccountCreateActivity::class.java)
            startActivity(intent)
        }

        binding.button.setOnClickListener {
            if(areFieldsFull()){
                Services.accountService.sendLoginRequest(this,binding.email.text.toString(),binding.password.text.toString())
            }
        }

        setContentView(binding.root)
    }

    fun areFieldsFull () : Boolean{
        val ret = binding.email.text.isNotBlank() && binding.password.text.isNotBlank()
        if(!ret)
            binding.warning.text = "Merci de remplir tous les champs"
        return ret
    }
}