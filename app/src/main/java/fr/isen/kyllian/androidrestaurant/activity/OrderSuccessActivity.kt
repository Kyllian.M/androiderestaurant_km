package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.databinding.ActivityOrderSuccessBinding

private lateinit var binding : ActivityOrderSuccessBinding
class OrderSuccessActivity : AppCompatActivityWMenuBar(), OnMapReadyCallback {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderSuccessBinding.inflate(layoutInflater)
        binding.button.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
        val map = binding.mapView
        map.onCreate(savedInstanceState)
        map.getMapAsync(this)
        setContentView(binding.root)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(43.1206991,5.939154))
                .title("Resto ISEN")
        )
    }
}