package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.service.Services
import java.lang.String


abstract class AppCompatActivityWMenuBar : AppCompatActivity() {
    var top_menu : Menu? = null
    var textCartItemCount: TextView? = null

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_button,menu)
        top_menu = menu

        var menuItem = menu!!.findItem(R.id.goto_cart)
        var actionView = menuItem.actionView
        textCartItemCount = actionView.findViewById<View>(R.id.cart_badge) as TextView
        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }


        refreshCart()
        return true;
    }

    override fun onResume() {
        super.onResume()
        refreshCart()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.goto_cart -> {
                val panierActivity = Intent(this, CartActivity::class.java)
                startActivity(panierActivity)
                true
            }
            R.id.login -> {
                Log.i("Logs","Clicked On login, opening view AccountLoginActivity");
                val intent = Intent(this,AccountLoginActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.maps -> {
                Log.i("Logs","Clicked On maps, opening view MapsActivity");
                val intent = Intent(this,MapsActivity::class.java)
                startActivity(intent)
                true
            }
            else -> {
                Log.i("logs",""+item.itemId)
                super.onOptionsItemSelected(item)
            }
        }
    }

    fun refreshCart() {
        if (textCartItemCount != null) {
            if (Services.cartService.getNumItems() === 0) {
                if (textCartItemCount!!.getVisibility() !== View.GONE) {
                    textCartItemCount!!.setVisibility(View.GONE)
                }
            } else {
                textCartItemCount!!.setText(
                    String.valueOf(
                        Math.min(
                            Services.cartService.getNumItems(),
                            99
                        )
                    )
                )
                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
                    textCartItemCount!!.setVisibility(View.VISIBLE)
                }
            }
        }
    }
}