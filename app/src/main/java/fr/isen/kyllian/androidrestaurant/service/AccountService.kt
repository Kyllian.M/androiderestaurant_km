package fr.isen.kyllian.androidrestaurant.service

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.JsonObject
import fr.isen.kyllian.androidrestaurant.activity.AccountCreateActivity
import fr.isen.kyllian.androidrestaurant.model.CreateResponseJSON
import fr.isen.kyllian.androidrestaurant.model.RequestCreateJSON
import fr.isen.kyllian.androidrestaurant.model.RequestLoginJSON
import org.json.JSONObject


class AccountService {
    val url_register = "http://test.api.catering.bluecodegames.com/user/register"
    val url_login = "http://test.api.catering.bluecodegames.com/user/login"

    var login_info : CreateResponseJSON? = null

    fun sendCreateRequest(context : Activity,firstname : String,lastname : String,address : String,email : String,password : String){
        val request = RequestCreateJSON(1,firstname,lastname,address,email,password)
        val queue = Volley.newRequestQueue(context).apply { start() }
        val params = JSONObject(Gson().toJson(request))
        val stringRequest = JsonObjectRequest(
            Request.Method.POST, url_register,params,
            Response.Listener<JSONObject> { response ->
                Log.i("json","Response is: $response")
                val gson = Gson()
                val json = gson.fromJson(response.toString(), CreateResponseJSON::class.java)
                login_info = json
                save(context)
                context.finish()
            },
            Response.ErrorListener {
                Log.i("test","request failed")
            }
        )
        queue.add(stringRequest)
    }

    fun sendLoginRequest(context : Activity,email : String,password : String){
        val request = RequestLoginJSON(1,email,password)
        val queue = Volley.newRequestQueue(context).apply { start() }
        val params = JSONObject(Gson().toJson(request))
        val stringRequest = JsonObjectRequest(
            Request.Method.POST, url_login,params,
            Response.Listener<JSONObject> { response ->
                Log.i("json","Response is: $response")
                val gson = Gson()
                val json = gson.fromJson(response.toString(), CreateResponseJSON::class.java)
                if(json.code != "NOK") {
                    login_info = json
                    save(context)
                    context.finish()
                }
            },
            Response.ErrorListener {
                Log.i("test","request failed")
            }
        )
        queue.add(stringRequest)
    }

    fun save(activity : Activity){
        val sharedPref = activity.getSharedPreferences("appref",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(PREF_KEY, Gson().toJson(login_info))
            apply()
            Log.i("logs","saved account JSON in user preferences")
        }
    }

    fun load(activity : Activity){
        val sharedPref = activity.getSharedPreferences("appref",Context.MODE_PRIVATE) ?: return
        val json : String? = sharedPref.getString(PREF_KEY, null)
        if(json == null){
            login_info = null
            Log.i("logs","No account JSON in user preferences")
        }
        else{
            login_info = Gson().fromJson(json, CreateResponseJSON::class.java)
            Log.i("logs","loaded account JSON from user preferences")
        }
    }

    companion object comp{
        val PREF_KEY : String = "login_info"
    }

}

//val accountService : AccountService = AccountService()