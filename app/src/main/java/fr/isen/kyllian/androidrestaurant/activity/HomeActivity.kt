package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import fr.isen.kyllian.androidrestaurant.databinding.ActivityHomeBinding
import fr.isen.kyllian.androidrestaurant.service.*
import java.io.File

private lateinit var binding: ActivityHomeBinding

class HomeActivity : AppCompatActivityWMenuBar() {

    private lateinit var btnEntree : android.widget.Button;
    private lateinit var btnPlat : android.widget.Button;
    private lateinit var btnDessert : android.widget.Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)

        btnEntree  = initMenuButton(binding.btnEntree );
        btnPlat    = initMenuButton(binding.btnPlat   );
        btnDessert = initMenuButton(binding.btnDessert);
        binding.propos.setOnClickListener {
            val intent = Intent(this,TrailerActivity::class.java)
            startActivity(intent)
        }

        Services.foodService = FoodService()
        Services.foodService.update(this)

        Services.cartService.sourceFile =  File(cacheDir.absolutePath  + "/../shoppingcart.json")
        Services.cartService.load()
        Services.accountService.load(this)
        setContentView(binding.root)
    }

    private fun initMenuButton(btn : android.widget.Button): Button {
        val activity_class = MenuListActivity::class.java;
        btn.setOnClickListener {
            val category = btn.text.toString().split(" ")[1]
            Log.i("Logs","Clicked On '" + category + "', opening view '" + activity_class.canonicalName + "'");
            val intent = Intent(this,activity_class)
            intent.putExtra("category",category)
            startActivity(intent)
        }
        return btn;
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("Logs","L'activité home s'est faite detruire :'(")
    }
}