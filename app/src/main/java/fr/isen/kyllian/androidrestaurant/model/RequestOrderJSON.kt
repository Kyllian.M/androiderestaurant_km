package fr.isen.kyllian.androidrestaurant.model

import java.io.Serializable

data class RequestOrderJSON (
    val id_shop : Int,
    val id_user : Int,
    val msg : String
) : Serializable