package fr.isen.kyllian.androidrestaurant.service

class Services {
    companion object{
        var foodService : FoodService = FoodService()
        val cartService : CartService = CartService()
        val accountService : AccountService = AccountService()
    }
}