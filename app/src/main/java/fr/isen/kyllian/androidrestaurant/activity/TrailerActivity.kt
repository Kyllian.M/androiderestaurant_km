package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.databinding.ActivityTrailerBinding


private lateinit var binding : ActivityTrailerBinding
class TrailerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrailerBinding.inflate(layoutInflater)
        binding.retour.setOnClickListener {
            val intent = Intent(this,HomeActivity::class.java)
            startActivity(intent)
        }
        lifecycle.addObserver(binding.activityMainYoutubePlayerView)
        setContentView(binding.root)
    }
}