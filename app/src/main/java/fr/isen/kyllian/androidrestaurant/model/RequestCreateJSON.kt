package fr.isen.kyllian.androidrestaurant.model

import java.io.Serializable

data class RequestCreateJSON (val id_shop : Int,val firstname : String,val lastname : String,val address : String,val email : String,val password : String) : Serializable