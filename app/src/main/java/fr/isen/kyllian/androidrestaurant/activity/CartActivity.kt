package fr.isen.kyllian.androidrestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import fr.isen.kyllian.androidrestaurant.adapters.CartListAdapter
import fr.isen.kyllian.androidrestaurant.databinding.ActivityCartBinding
import fr.isen.kyllian.androidrestaurant.service.Services

private lateinit var binding: ActivityCartBinding
class CartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartBinding.inflate(layoutInflater)
        binding.recycler.layoutManager = LinearLayoutManager(this)
        binding.recycler.adapter = CartListAdapter(Services.cartService.lots.items)
        setContentView(binding.root)

        binding.btnBuy.setOnClickListener {
            if(Services.accountService.login_info == null) {
                Log.i("Logs", "Clicked On '" + binding.btnBuy.text + "', opening view AccountCreateActivity");
                val intent = Intent(this, AccountLoginActivity::class.java)
                startActivity(intent)
            }
            else{
                Services.cartService.placeOrder(this);
            }
        }

        binding.total.text = Services.cartService.getFullPrice().toString() + "€"
    }
}
