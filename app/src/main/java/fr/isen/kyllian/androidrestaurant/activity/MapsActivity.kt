package fr.isen.kyllian.androidrestaurant.activity

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.isen.kyllian.androidrestaurant.R
import fr.isen.kyllian.androidrestaurant.databinding.ActivityMapsBinding

private lateinit var binding : ActivityMapsBinding
class MapsActivity : AppCompatActivityWMenuBar() , OnMapReadyCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)

        val map = binding.mapView
        map.onCreate(savedInstanceState)
        map.getMapAsync(this)

        setContentView(binding.root)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(43.1206991,5.939154))
                .title("Resto ISEN")
        )
    }
}