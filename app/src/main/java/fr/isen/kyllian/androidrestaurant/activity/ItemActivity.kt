package fr.isen.kyllian.androidrestaurant.activity

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import com.squareup.picasso.Picasso
import fr.isen.kyllian.androidrestaurant.adapters.DetailViewAdapter
import fr.isen.kyllian.androidrestaurant.databinding.ActivityItemBinding
import fr.isen.kyllian.androidrestaurant.model.FoodJSON
import fr.isen.kyllian.androidrestaurant.service.Services

private lateinit var binding: ActivityItemBinding;

class ItemActivity : AppCompatActivityWMenuBar() {
    private lateinit var item : FoodJSON

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemBinding.inflate(layoutInflater)
        item = Services.foodService.getFoodById(intent.getIntExtra(("id"),-1))

        binding.itemName.text = item.name_fr
        if(item.images.size > 0 && item.images[0].isNotBlank()) {
            //Picasso.get().load(item.images[0]).into(binding.foodImage)
            binding.foodImage.setVisibility(View.GONE)
            var it = item.images
            binding.vierwpager.adapter = DetailViewAdapter(this, it)
        }
        binding.price.text = "${item.prices[0].price}€"
        binding.totalsum.text = "0€"
        binding.categoryName.text = item.categ_name_fr
        binding.itemDesc.text = Services.foodService.ingredientsText(item)
        binding.btnBuy.setOnClickListener {
            if(binding.qttInput.text.isNotBlank()) {
                val nb = binding.qttInput.text.toString().toInt()
                Services.cartService.add(item, nb)
                Log.i("logs", "added $nb of ${item.name_fr} to cart, cart total is now ${Services.cartService.getFullPrice()} euros")
                refreshCart()
                setQttMessage()
            }
        }
        setQttMessage()

        binding.qttInput.setOnKeyListener { _: View, _: Int, _: KeyEvent ->
            try {
                val op = item.prices[0].price.toFloat() * binding.qttInput.text.toString().toFloat()
                binding.totalsum.text = "$op €"
            } catch (e: NumberFormatException) {
                //Not a number inputed
                binding.totalsum.text = "0€"
            }
            false
        }

        setContentView(binding.root);
    }

    fun setQttMessage(){
        val nb = Services.cartService.howManyOf(item.id)
        if(nb > 0){
            binding.warning.text = "⚠️Deja $nb exemplaires dans le panier ⚠️"
        }
        else
            binding.warning.text = ""
    }
}

